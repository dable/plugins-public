# How to use Dable for Ameba plugin

This documentation explains how to add native advertising to your posts on Ameblo using the dedicated plugin.

## Setup

The Dable for Ameba could be installed through the custom plugin.

### 1. Go into the administration area of your blog, then move to Settings > Add plugins page.

![](./images/1-1.jpg)
![](./images/1-2.jpg)

### 2. Click on the custom plugin tab.

![](./images/2.jpg)

### 3. Insert the following code into the text field and push the Save button to store it.

```
<script id="dable-for-ameba" src="THE_URL_OF_THE_PLUGIN" data-widget-id="YOUR_WIDGET_ID" defer></script>
```

If you don't know the widget ID, please contact the customer service(<support@dable.io>).

### 4. Enter the sidebar settings page by clicking on the "Here(こちら)" link and make sure the custom plugin you just added is located in the "使用する機能" section. If not, you should drag and drop it into the area.

![](./images/4-1.jpg)
![](./images/4-2.jpg)

Don't forget to press the Save button after done.

## Check it out

Note that it may take from 2 to 7 days to get the widget working, depending on web traffic of your blog.
But you can check if the plugin is successfully applied to your blog in the Dable Dashboard anytime. Select Log Status > Log Aggregation Status, you will see some real time logs on the page if the plugin is working.
