requireDable = ->
  ((d,a,b,l,e,_) ->
    d[b]=d[b]||(->(d[b].q=d[b].q||[]).push(arguments));e=a.createElement(l)
    e.async=1;e.charset='utf-8';e.src='//static.dable.io/dist/plugin.min.js'
    _=a.getElementsByTagName(l)[0];_.parentNode.insertBefore(e,_)
  )(window,document,'dable','script')

getService = (loc)->
  return loc.host + (/^\/[^\/]+/.exec(loc.pathname)||[''])[0]

getItemId = ->
  s = window.location.pathname.split("/")
  return s[s.length - 1].match(/^[0-9]+$/)?[0] || null

cookContent = ->
  meta = {}
  # dable:item_id meta
  meta['dable:item_id'] = document.querySelector('[data-unique-entry-id]')?.getAttribute 'data-unique-entry-id'
  # article:published_time meta
  pubDate = document.querySelector('[data-uranus-component="entryDate"] time')?.innerText
  meta['article:published_time'] = pubDate.replace(' ','T') + '+0900' 
  # article:section
  meta['article:section'] = document.querySelector('[data-uranus-component="entryThemes"] a[rel="tag"]')?.innerText
  # generate meta tags
  document.head.insertAdjacentHTML 'beforeend', "<meta property=\"#{prop}\" content=\"#{content}\">" for prop, content of meta when content
  # add itemprop="articleBody" to the post body
  document.querySelector('div[data-uranus-component="entryBody"]')?.setAttribute 'itemprop', 'articleBody'

createWidget = ->
  script = document.querySelector '#dable-for-ameba'
  widgetId = script?.getAttribute 'data-widget-id'
  postBodies = document.querySelectorAll '[data-uranus-component="entryBody"]' if widgetId
  for postBody, i in postBodies
    postBody.insertAdjacentHTML 'afterend', "<div id='dablewidget_#{ widgetId }_#{ i }' data-widget_id='#{ widgetId }'></div>"
    dable 'renderWidget', "dablewidget_#{ widgetId }_#{ i }";

requireDable()
dable('setService', getService(window.location))

item_id = getItemId()

if item_id
  cookContent()
  dable('sendLog', "view", item_id)
else
  dable('sendLog', "visit")

createWidget()