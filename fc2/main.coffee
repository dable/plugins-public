requireDable = ->
  ((d,a,b,l,e,_) ->
    d[b]=d[b]||(->(d[b].q=d[b].q||[]).push(arguments));e=a.createElement(l)
    e.async=1;e.charset='utf-8';e.src='//static.dable.io/dist/plugin.min.js'
    _=a.getElementsByTagName(l)[0];_.parentNode.insertBefore(e,_)
  )(window,document,'dable','script')

getService = (loc)->
  return loc.host

getItemId = ->
  return /\/blog-entry-(\d+)\.html$/.exec(location.pathname)?[1]

widgetIndex = 0
createWidget = ->
  scripts = document.querySelectorAll '#dable-for-fc2'
  for script, i in scripts
  	widgetIndex++
  	widgetId = script.getAttribute 'data-widget-id'
  	script.removeAttribute('id')
  	script.insertAdjacentHTML 'afterend', "<div id='dablewidget_#{ widgetId }_#{ widgetIndex }' data-widget_id='#{ widgetId }'></div>"
  	dable 'renderWidget', "dablewidget_#{ widgetId }_#{ widgetIndex }";

requireDable()
dable('setService', getService(window.location))

item_id = getItemId()
if item_id
  dable('sendLog', "view", item_id)
else
  dable('sendLog', "visit")

createWidget()