# How to use Dable for FC2 plugin

This documentation explains how to add native advertising to your posts on FC2 blog using the dedicated plugin.

## Get started

To use Dable for FC2, customizing templates is required. Go into the administration panels of your blog, then move to Templates and click on the Edit link on the same row of your current theme, which is flagged.

![](./images/get-started.png)

## Edit the current template

Enter the edit field in the area **Edit HTML** by clicking it and press `Ctrl+F` (`Cmd+F` for OSX) then find `<%topentry_body>`.

![](./images/click-and-find.gif)

Replace `<%topentry_body>` with the following code.

```html
<!--permanent_area-->
<meta property="dable:item_id" content="<%topentry_no>" />
<meta property="dable:title" content="<%topentry_title%>" />
<meta property="article:section" content="<%topentry_category>" />
<meta property="article:published_time" content="<%topentry_year>-<%topentry_month>-<%topentry_day>T<%topentry_hour>:<%topentry_minute>:<%topentry_second>+0900">
<!--/permanent_area-->
<div itemprop="articleBody"><%topentry_body></div>
<script src="//static.dable.io/dist/fc2.min.js" id="dable-for-fc2" data-widget-id="YOUR_WIDGET_ID" defer></script>
```

You should use your own widget ID we might already send you instead of `YOUR_WIDGET_ID`.
In case you don't know about it, please contact the customer service(<support@dable.io>).

Now press the Update button to apply the changes to your blog.

![](./images/save.png)

## Check it out

Note that it may take from 2 to 7 days to get the widget working, depending on web traffic of your blog.
But you can check if the plugin is successfully applied to your blog in the Dable Dashboard anytime. Select Log Status > Log Aggregation Status, you will see some real time logs on the page if the plugin is working.
