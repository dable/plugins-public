# How to use Dable for Livedoor plugin

This documentation explains how to add native advertising to your posts on a Livedoor blog using the dedicated plugin.

## Setup

The Dable for Livedoor could be installed through the フリーエリア widget.

### 1. Go into the administration area of your blog, then move to ブログ設定 > PC in the デザイン / ブログパーツ設定 section.

![](./images/1-1.jpg)
![](./images/1-2.jpg)

### 2. Click on the ブログパーツ tab.

![](./images/2.jpg)

### 3. Drag and drop フリーエリア widget into either A or B area.

![](./images/3.jpg)

### 4. Mouse hover on the widget you just added then click on the 設定 button. You will see a popup dialog.

![](./images/4.jpg)

### 5. Insert the following code into the biggest text field and push the Save button to store it. Please keep the ラベル empty.

```
<script id="dable-for-livedoor" src="THE_URL_OF_THE_PLUGIN" data-widget-id="YOUR_WIDGET_ID" defer></script>
```

![](./images/5.jpg)

If you don't know the widget ID, please contact the customer service(<support@dable.io>). Again, don't forget to press the Save button after done

## Check it out

Note that it may take from 2 to 7 days to get the widget working, depending on web traffic of your blog.
But you can check if the plugin is successfully applied to your blog in the Dable Dashboard anytime. Select Log Status > Log Aggregation Status, you will see some real time logs on the page if the plugin is working.
