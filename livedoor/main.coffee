requireDable = ->
  ((d,a,b,l,e,_) ->
    d[b]=d[b]||(->(d[b].q=d[b].q||[]).push(arguments));e=a.createElement(l)
    e.async=1;e.charset='utf-8';e.src='//static.dable.io/dist/plugin.min.js'
    _=a.getElementsByTagName(l)[0];_.parentNode.insertBefore(e,_)
  )(window,document,'dable','script')

getService = (loc)->
  if loc.host != 'blog.livedoor.jp'
    return loc.host
  return loc.host + (/^\/[^\/]+/.exec(loc.pathname)||[''])[0]

getItemId = ->
  return /archives\/(\d+)\.html$/.exec(location.pathname)?[1]

cookContent = ->
  meta = {}
  # dable:item_id meta
  meta['dable:item_id'] = getItemId()
  # dable:title - only for single post
  title = document.querySelectorAll '.article-title,[itemprop="name"]'
  meta['dable:title'] = title[0].innerText if meta['dable:item_id'] and title and title.length == 1
  # article:published_time meta
  meta['article:published_time'] = document.querySelector('time[itemprop="datePublished"]')?.getAttribute 'datetime'
  # article:section
  meta['article:section'] = document.querySelector('.article-category1 a')?.innerText
  # generate meta tags
  document.head.insertAdjacentHTML 'beforeend', "<meta property=\"#{prop}\" content=\"#{content}\">" for prop, content of meta when content
  # articleBody
  if not document.querySelector 'div[itemprop="articleBody"]'
    document.querySelector('div.article-body')?.setAttribute('itemprop', 'articleBody')

createWidget = ->
  widgetId = document.querySelector('#dable-for-livedoor')?.getAttribute 'data-widget-id'
  postBodies = document.querySelectorAll 'div[itemprop="articleBody"],div.article-body' if widgetId
  for postBody, i in postBodies
    postBody.insertAdjacentHTML 'afterend', "<div id='dablewidget_#{ widgetId }_#{ i }' data-widget_id='#{ widgetId }'></div>"
    dable 'renderWidget', "dablewidget_#{ widgetId }_#{ i }";

requireDable()
dable('setService', getService(window.location))

item_id = getItemId()
if item_id
  cookContent()
  dable('sendLog', "view", item_id)
else
  dable('sendLog', "visit")

createWidget()