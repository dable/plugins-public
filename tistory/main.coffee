requireDable = ->
  ((d,a,b,l,e,_) ->
    d[b]=d[b]||(->(d[b].q=d[b].q||[]).push(arguments));e=a.createElement(l)
    e.async=1;e.charset='utf-8';e.src='//static.dable.io/dist/plugin.min.js'
    _=a.getElementsByTagName(l)[0];_.parentNode.insertBefore(e,_)
  )(window,document,'dable','script')

getService = ->
  isMobile = window.location.pathname == '/m' || window.location.pathname.indexOf('/m/') > -1
  return window.location.host + (isMobile && "/m" || "")

getItemId = ->
  s = window.location.pathname.split("/")
  return s[s.length - 1].match(/^[0-9]+$/)?[0] || null

requireDable()
dable('setService', getService())

item_id = getItemId()
if item_id
  dable('sendLog', "view", item_id)
else
  dable('sendLog', "visit")
